# Static Code Analysis for All Languages - coala!

coala is an application that makes it very easy, writing analysis for any
programming language or even arbitrary textual data. It is a useful abstraction
that provides a convenient user interface and takes away a lot of common tasks
from the algorithm developer, effectively making bare research available for
production use.

This talk features a short introduction into the thoughts behind coala, it's
ability to speed up research as well as increase productivity and an
interactive demonstration of our command line and other interfaces - bring a
laptop or just watch the show!
