# Contributing to an Open Source Project

This workshop introduces participants to some common workflows used in major
open source projects.

## Goals

Participants will have a commit in an actual open source project and will be
able to apply the knowledge to other projects and get started with free
software.

## Requirements

Light git knowledge is recommended.

## Schedule

Time         |Topic
-------------|------------------------------------------
11:00 - 11:15|Introduction
11:15 - 11:35|Who is Who (*)
11:35 - 11:45|Welcome to the Org and Channel (*)
11:45 - 11:55|Grab an Issue (*)
11:55 - 12:10|Install and Configure Git (*)
12:10 - 12:15|Fork and Pull
12:15 - 12:25|Fork and Pull (*)
12:25 - 12:35|Commit, Message, Push and PR
12:35 - 13:00|Code, Commit, Push and PR (*)
13:00 - 13:10|Code Reviews, Commit Messages
13:10 - 13:30|Iterate (*)
13:30 - 13:40|Rebase
13:40 - 13:55|Other Workflows - Discussion (*)
13:55 - 14:00|Feedback
