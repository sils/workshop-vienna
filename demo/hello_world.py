
def main():
	"""
	This is a simple main routine. A tutorial is available at
	http://docs.python.org/3.5/tutorial
	"""
	print("Hello world!")

if __name__ == "__main__":
	main()
