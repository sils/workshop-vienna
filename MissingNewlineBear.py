from coalib.bears.LocalBear import LocalBear
from coalib.results.Diff import Diff
from coalib.results.Result import Result


class MissingNewlineBear(LocalBear):

    LANGUAGES = "All"

    def run(self, filename, file, newline_before_eof: bool):
        """
        Check for a newline before end of file.

        :param newline_before_eof: True if the newline should exist before the
                                   end of the file.
        """
        if newline_before_eof:
            if file[-1][-1] != '\n':
                diff = Diff(file)
                diff.change_line(len(file), file[-1], file[-1]+'\n')
                yield Result.from_values(
                    self,
                    "Missing newline before EOF.",
                    file=filename,
                    line=len(file),
                    diffs={filename: diff})
        else:
            if file[-1][-1] == '\n':
                diff = Diff(file)
                diff.change_line(len(file), file[-1], file[-1][:-1])
                yield Result.from_values(
                    self,
                    "There's a newline before EOF but shouldn't be! :(",
                    file=filename,
                    line=len(file),
                    diffs={filename: diff})